# README #


### What is this repository for? ###

* Tool used for parsing MusicXML files and extracting useful information for use with EC algorithms.
* Version 0.1

### How do I get set up? ###

* Depends on sqlite-jdbc-3.7.2.jar, MUST be in your classpath
* Use as jar file is recommended


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner

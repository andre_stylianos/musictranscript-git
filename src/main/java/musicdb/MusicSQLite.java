package musicdb;

import java.io.File;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import java.io.FileNotFoundException;
import javax.xml.stream.XMLStreamException;

import xmlMusicFiles.MusicxmlParser;

public class MusicSQLite implements Runnable{
	//	private static Connection c = null;
	private static String sql = "INSERT OR IGNORE INTO MUSIC VALUES(?,?,?);";
	private File f;
	private Connection c;
	private PreparedStatement ps;
	private ExecutorService service;
	private ReentrantLock lock = new ReentrantLock();
	private static Map<File,Integer> mapaquantidade = new HashMap<File,Integer>();

//	public MusicSQLite(){
//		try {
//			c=openConnection();
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		service = Executors.newFixedThreadPool(20);
//	}
//
	/**
	 * creates a musicsql object which makes use of threads
	 * @param thread the number of threads to use
	 */
	public MusicSQLite(int thread){
		try {
			c=openConnection();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		service = Executors.newFixedThreadPool(thread);
	}

	private MusicSQLite(File f){
		this.f=f;
	}
	private void recursiveInsertion(File folder)
			throws SQLException {
		File[] files = folder.listFiles();
		for(File f: files){
			if((f.isFile())&&(!musicExists(f))){
				if(f.getName().endsWith(".xml")){
					service.submit(new MusicSQLite(f));
//										insertFile(f);
				}
			}
			if(f.isDirectory()){
				recursiveInsertion(f);
			}
		}
	}


	private void insertFile(File f)
			throws SQLException {
		try{
			int notequantity;
			MusicxmlParser xmlparser = new MusicxmlParser();
			notequantity=xmlparser.readXml(f.getAbsolutePath()).getNoteCount();
			ps = c.prepareStatement(sql);
			String genre;
			System.out.println(f.getName()+" sendo adicionada...");
			ps.setString(1, f.getName());
			ps.setInt(2,notequantity);
			String[] musicpath = f.getParent().split(File.separator);
			genre = musicpath[musicpath.length-1];
			if(genre.equals("libxml")) genre = "Unknown";
			ps.setString(3, genre);
			ps.executeUpdate();
			ps.close();
		}catch(Exception e){
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		}
	}

	private void closeConnection(Connection c)
			throws SQLException {
		c.commit();
		c.close();
	}


	/**
	 * checks if a music file exists in the db
	 * @param arquivo the file to be checked (only file name will be used in the search)
	 * @return true if the music exists, false if it doesn't
	 */
	public boolean musicExists(File arquivo){
		PreparedStatement ps = null;
		Connection con=null;
		String sql = "SELECT * FROM MUSIC WHERE TITLE = ?;";
		try{
			con=openConnection();
			ps = con.prepareStatement(sql);
			ps.setString(1, arquivo.getName());
			ResultSet result = ps.executeQuery();
			if(result.next()){
				System.out.println("Musica "+arquivo.getName()+" ja existe, pulando...");
				ps.close();
				closeConnection(con);
				return true;
			}else{
				closeConnection(con);
				ps.close();
				return false;
			}
		}catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.err.println("Houve um erro ao procurar a musica no banco de dados");
		return true;

	}

	private Connection openConnection() throws SQLException, ClassNotFoundException{
		Connection c = null;
		Class.forName("org.sqlite.JDBC");
		c = DriverManager.getConnection("jdbc:sqlite:library.db");
		c.setAutoCommit(false);
		return c;
	}

	/**
	 * includes a musicxml file or, if a dir is given, recursively adds all musicxml files found
	 * @param file musicxml file or dir which contains only musicxml files
	 */
	public void insertMusic(File file){
		if(file.isDirectory()){
			try {
				recursiveInsertion(file);
				service.shutdown();
				try{
					service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
				}catch(InterruptedException e){
					e.printStackTrace();
				}
				insertBatch();
				closeConnection(c);
			} catch ( Exception e ) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				System.exit(0);
			}
		}else if((file.isFile())&&(!musicExists(file))){
			try {
				insertFile(file);
				closeConnection(c);
			} catch ( Exception e ) {
				System.err.println( e.getClass().getName() + ": " + e.getMessage() );
				System.exit(0);
			}
		}
		;
	}

	/**
	 * creates a table in the database if it doesn't exist
	 */
	public void createTable(){
		PreparedStatement ps = null;
		Connection c=null;
		String sql = "CREATE TABLE IF NOT EXISTS MUSIC(TITLE TEXT PRIMARY KEY, NOTEQUANTITY INT, GENRE VARCHAR(15));";
		try{
			c = openConnection();
			ps = c.prepareStatement(sql);
			ps.executeUpdate();
			ps.close();
			closeConnection(c);
		}catch(Exception e){
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	/**
	 * prints the database content ordered by notequantity
	 */
	public void printOrdered(){
		PreparedStatement ps = null;
		Connection c;
		String sql = "SELECT * FROM MUSIC ORDER BY NOTEQUANTITY;";
		try{
			c = openConnection();
			ps = c.prepareStatement(sql);
			ResultSet result = ps.executeQuery();
			while(result.next()){
				System.out.println("Musica = "+result.getString(1)+"\t\tNotas = "+result.getString(2))	;
			}
			ps.close();
			closeConnection(c);
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	private void readFile(File f){
		int notequantity;
		System.out.println("Parseando "+f.getName()+"...");
		MusicxmlParser xmlparser = new MusicxmlParser();
		try{
		notequantity=xmlparser.readXml(f.getAbsolutePath()).getNoteCount();
		mapaquantidade.put(f, notequantity);
		}catch(XMLStreamException e){
			System.out.println("Musica "+f.getName()+" invalida, pulando...");
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
	}

	private void insertBatch(){
		try{
			ps = c.prepareStatement(sql);
			for(Entry<File, Integer> mapa : mapaquantidade.entrySet()){
				String genre;
				System.out.println(mapa.getKey().getName()+" sendo adicionada...");
				ps.setString(1, mapa.getKey().getName());
				ps.setInt(2,mapa.getValue());
				String[] musicpath = mapa.getKey().getParent().split(File.separator);
				genre = musicpath[musicpath.length-1];
				if(genre.equals("libxml")) genre = "Unknown";
				ps.setString(3, genre);
				ps.addBatch();
			}
			ps.executeBatch();
		}catch(Exception e){
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	@Override
	public void run() {
		readFile(this.f);
	}
}

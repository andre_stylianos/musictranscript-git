/**
 * Class structure for use as an application all by itself
 */
package musictranscript;

import java.io.File;

import org.omg.PortableInterceptor.USER_EXCEPTION;

import musicdb.MusicSQLite;

public class MusicTranscript {

	public static void main(String[] args) {
		String caminho=null;
		File musicxml=null;
		int thread=20;
		if(args.length==0){
			System.out.println("XML partition to tablature transcription v0.1");
			System.out.println("-xml\t especificar caminho do arquivo musicxml contendo a partitura");
			System.out.println("-dir\t especificar o caminho de um diretorio contendo apenas arquivos musicxml");
			System.out.println("-db\t inicializa o bd caso este nao esteja inicializado");
			System.out.println("-print\t imprime as musicas do bd ordenadas por quantidade de notas");
			System.out.println("-thread\t define o numero de threads utilizados. CASO NAO ESPECIFICADO, DEFAULT = 20");
			System.exit(0);
		}else{
			for(int i=0;i<args.length;i+=2){
				if(args[i].equals("-thread")){
					thread=Integer.parseInt(args[i+1]);
				}
			}
			MusicSQLite musicsql = new MusicSQLite(thread);
			for(int i=0;i<args.length;i+=2){
				if((args[i]).equals("-xml")){
					caminho = args[i+1];
					musicxml = new File(caminho);
					if(!musicxml.isFile()){
						System.err.println(args[i+1]+" não é um arquivo.");
						System.exit(0);
					}else{
						musicsql.insertMusic(musicxml);
					}
					continue;
				}
				if(args[i].equals("-dir")){
					caminho = args[i+1];
					musicxml = new File(caminho);
					if(!musicxml.isDirectory()){
						System.err.println(args[i+1]+" não é um diretório.");
						System.exit(0);
					}else{
						musicsql.insertMusic(musicxml);
					}
				}
				if(args[i].equals("-db")){
					musicsql.createTable();
				}
				if(args[i].equals("-print")){
					musicsql.printOrdered();
				}
			}
		}
	}
}

package xmlMusicFiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Music {
//	/**
//	 * @author João Ramos
//	 */
//    private static int[][] matrizPosicoesNotas = new int[][]{
//        {40, 6, 0, -1, -1, -1, -1, -1, -1, -1, -1, 4, 3},
//        {41, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, 5, 3},
//        {42, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, 6, 3},
//        {43, 6, 3, -1, -1, -1, -1, -1, -1, -1, -1, 7, 3},
//        {44, 6, 4, -1, -1, -1, -1, -1, -1, -1, -1, 8, 3},
//        {45, 6, 5, 5, 0, -1, -1, -1, -1, -1, -1, 9, 3},
//        {46, 6, 6, 5, 1, -1, -1, -1, -1, -1, -1, 10, 3},
//        {47, 6, 7, 5, 2, -1, -1, -1, -1, -1, -1, 11, 3},
//        {48, 6, 8, 5, 3, -1, -1, -1, -1, -1, -1, 0, 4},
//        {49, 6, 9, 5, 4, -1, -1, -1, -1, -1, -1, 1, 4},
//        {50, 6, 10, 5, 5, 4, 0, -1, -1, -1, -1, 2, 4},
//        {51, 6, 11, 5, 6, 4, 1, -1, -1, -1, -1, 3, 4},
//        {52, 6, 12, 5, 7, 4, 2, -1, -1, -1, -1, 4, 4},
//        {53, 6, 13, 5, 8, 4, 3, -1, -1, -1, -1, 5, 4},
//        {54, 6, 14, 5, 9, 4, 4, -1, -1, -1, -1, 6, 4},
//        {55, 6, 15, 5, 10, 4, 5, 3, 0, -1, -1, 7, 4},
//        {56, 6, 16, 5, 11, 4, 6, 3, 1, -1, -1, 8, 4},
//        {57, 6, 17, 5, 12, 4, 7, 3, 2, -1, -1, 9, 4},
//        {58, 6, 18, 5, 13, 4, 8, 3, 3, -1, -1, 10, 4},
//        {59, 6, 19, 5, 14, 4, 9, 3, 4, 2, 0, 11, 4},
//        {60, 6, 20, 5, 15, 4, 10, 3, 5, 2, 1, 0, 5},
//        {61, 5, 16, 4, 11, 3, 6, 2, 2, -1, -1, 1, 5},
//        {62, 5, 17, 4, 12, 3, 7, 2, 3, -1, -1, 2, 5},
//        {63, 5, 18, 4, 13, 3, 8, 2, 4, -1, -1, 3, 5},
//        {64, 5, 19, 4, 14, 3, 9, 2, 5, 1, 0, 4, 5},
//        {65, 5, 20, 4, 15, 3, 10, 2, 6, 1, 1, 5, 5},
//        {66, 4, 16, 3, 11, 2, 7, 1, 2, -1, -1, 6, 5},
//        {67, 4, 17, 3, 12, 2, 8, 1, 3, -1, -1, 7, 5},
//        {68, 4, 18, 3, 13, 2, 9, 1, 4, -1, -1, 8, 5},
//        {69, 4, 19, 3, 14, 2, 10, 1, 5, -1, -1, 9, 5},
//        {70, 4, 20, 3, 15, 2, 11, 1, 6, -1, -1, 10, 5},
//        {71, 3, 16, 2, 12, 1, 7, -1, -1, -1, -1, 11, 5},
//        {72, 3, 17, 2, 13, 1, 8, -1, -1, -1, -1, 0, 6},
//        {73, 3, 18, 2, 14, 1, 9, -1, -1, -1, -1, 1, 6},
//        {74, 3, 19, 2, 15, 1, 10, -1, -1, -1, -1, 2, 6},
//        {75, 3, 20, 2, 16, 1, 11, -1, -1, -1, -1, 3, 6},
//        {76, 2, 17, 1, 12, -1, -1, -1, -1, -1, -1, 4, 6},
//        {77, 2, 18, 1, 13, -1, -1, -1, -1, -1, -1, 5, 6},
//        {78, 2, 19, 1, 14, -1, -1, -1, -1, -1, -1, 6, 6},
//        {79, 2, 20, 1, 15, -1, -1, -1, -1, -1, -1, 7, 6},
//        {80, 1, 16, -1, -1, -1, -1, -1, -1, -1, -1, 8, 6},
//        {81, 1, 17, -1, -1, -1, -1, -1, -1, -1, -1, 9, 6},
//        {82, 1, 18, -1, -1, -1, -1, -1, -1, -1, -1, 10, 6},
//        {83, 1, 19, -1, -1, -1, -1, -1, -1, -1, -1, 11, 6},
//        {84, 1, 20, -1, -1, -1, -1, -1, -1, -1, -1, 0, 7}};
//
//    /**
//     * @author joão Ramos
//     */
//    private static int[][] matrizIdPosicoes = new int[][]{
//        {0, 0, 0},
//        {1, 1, 0},
//        {2, 1, 1},
//        {3, 1, 2},
//        {4, 1, 3},
//        {5, 1, 4},
//        {6, 1, 5},
//        {7, 1, 6},
//        {8, 1, 7},
//        {9, 1, 8},
//        {10, 1, 9},
//        {11, 1, 10},
//        {12, 1, 11},
//        {13, 1, 12},
//        {14, 1, 13},
//        {15, 1, 14},
//        {16, 1, 15},
//        {17, 1, 16},
//        {18, 1, 17},
//        {19, 1, 18},
//        {20, 1, 19},
//        {21, 1, 20},
//        {22, 2, 0},
//        {23, 2, 1},
//        {24, 2, 2},
//        {25, 2, 3},
//        {26, 2, 4},
//        {27, 2, 5},
//        {28, 2, 6},
//        {29, 2, 7},
//        {30, 2, 8},
//        {31, 2, 9},
//        {32, 2, 10},
//        {33, 2, 11},
//        {34, 2, 12},
//        {35, 2, 13},
//        {36, 2, 14},
//        {37, 2, 15},
//        {38, 2, 16},
//        {39, 2, 17},
//        {40, 2, 18},
//        {41, 2, 19},
//        {42, 2, 20},
//        {43, 3, 0},
//        {44, 3, 1},
//        {45, 3, 2},
//        {46, 3, 3},
//        {47, 3, 4},
//        {48, 3, 5},
//        {49, 3, 6},
//        {50, 3, 7},
//        {51, 3, 8},
//        {52, 3, 9},
//        {53, 3, 10},
//        {54, 3, 11},
//        {55, 3, 12},
//        {56, 3, 13},
//        {57, 3, 14},
//        {58, 3, 15},
//        {59, 3, 16},
//        {60, 3, 17},
//        {61, 3, 18},
//        {62, 3, 19},
//        {63, 3, 20},
//        {64, 4, 0},
//        {65, 4, 1},
//        {66, 4, 2},
//        {67, 4, 3},
//        {68, 4, 4},
//        {69, 4, 5},
//        {70, 4, 6},
//        {71, 4, 7},
//        {72, 4, 8},
//        {73, 4, 9},
//        {74, 4, 10},
//        {75, 4, 11},
//        {76, 4, 12},
//        {77, 4, 13},
//        {78, 4, 14},
//        {79, 4, 15},
//        {80, 4, 16},
//        {81, 4, 17},
//        {82, 4, 18},
//        {83, 4, 19},
//        {84, 4, 20},
//        {85, 5, 0},
//        {86, 5, 1},
//        {87, 5, 2},
//        {88, 5, 3},
//        {89, 5, 4},
//        {90, 5, 5},
//        {91, 5, 6},
//        {92, 5, 7},
//        {93, 5, 8},
//        {94, 5, 9},
//        {95, 5, 10},
//        {96, 5, 11},
//        {97, 5, 12},
//        {98, 5, 13},
//        {99, 5, 14},
//        {100, 5, 15},
//        {101, 5, 16},
//        {102, 5, 17},
//        {103, 5, 18},
//        {104, 5, 19},
//        {105, 5, 20},
//        {106, 6, 0},
//        {107, 6, 1},
//        {108, 6, 2},
//        {109, 6, 3},
//        {110, 6, 4},
//        {111, 6, 5},
//        {112, 6, 6},
//        {113, 6, 7},
//        {114, 6, 8},
//        {115, 6, 9},
//        {116, 6, 10},
//        {117, 6, 11},
//        {118, 6, 12},
//        {119, 6, 13},
//        {120, 6, 14},
//        {121, 6, 15},
//        {122, 6, 16},
//        {123, 6, 17},
//        {124, 6, 18},
//        {125, 6, 19},
//        {126, 6, 20}};
//    
//    /**
//     * @author João Ramos
//     */
//    private static int[][] matrizPosicoesNotasId = new int[][]{
//        {40, 106, -1, -1, -1, -1, 4, 3},
//        {41, 107, -1, -1, -1, -1, 5, 3},
//        {42, 108, -1, -1, -1, -1, 6, 3},
//        {43, 109, -1, -1, -1, -1, 7, 3},
//        {44, 110, -1, -1, -1, -1, 8, 3},
//        {45, 111, 85, -1, -1, -1, 9, 3},
//        {46, 112, 86, -1, -1, -1, 10, 3},
//        {47, 113, 87, -1, -1, -1, 11, 3},
//        {48, 114, 88, -1, -1, -1, 0, 4},
//        {49, 115, 89, -1, -1, -1, 1, 4},
//        {50, 116, 90, 64, -1, -1, 2, 4},
//        {51, 117, 91, 65, -1, -1, 3, 4},
//        {52, 118, 92, 66, -1, -1, 4, 4},
//        {53, 119, 93, 67, -1, -1, 5, 4},
//        {54, 120, 94, 68, -1, -1, 6, 4},
//        {55, 121, 95, 69, 43, -1, 7, 4},
//        {56, 122, 96, 70, 44, -1, 8, 4},
//        {57, 123, 97, 71, 45, -1, 9, 4},
//        {58, 124, 98, 72, 46, -1, 10, 4},
//        {59, 125, 99, 73, 47, 22, 11, 4},
//        {60, 126, 100, 74, 48, 23, 0, 5},
//        {61, 101, 75, 49, 24, -1, 1, 5},
//        {62, 102, 76, 50, 25, -1, 2, 5},
//        {63, 103, 77, 51, 26, -1, 3, 5},
//        {64, 104, 78, 52, 27, 1, 4, 5},
//        {65, 105, 79, 53, 28, 2, 5, 5},
//        {66, 80, 54, 29, 3, -1, 6, 5},
//        {67, 81, 55, 30, 4, -1, 7, 5},
//        {68, 82, 56, 31, 5, -1, 8, 5},
//        {69, 83, 57, 32, 6, -1, 9, 5},
//        {70, 84, 58, 33, 7, -1, 10, 5},
//        {71, 59, 34, 8, -1, -1, -1, 11, 5},
//        {72, 60, 35, 9, -1, -1, -1, 0, 6},
//        {73, 61, 36, 10, -1, -1, -1, 1, 6},
//        {74, 62, 37, 11, -1, -1, -1, 2, 6},
//        {75, 63, 38, 12, -1, -1, -1, 3, 6},
//        {76, 39, 13, -1, -1, -1, -1, -1, 4, 6},
//        {77, 40, 14, -1, -1, -1, -1, -1, 5, 6},
//        {78, 41, 15, -1, -1, -1, -1, -1, 6, 6},
//        {79, 42, 16, -1, -1, -1, -1, -1, 7, 6},
//        {80, 17, -1, -1, -1, -1, -1, -1, -1, 8, 6},
//        {81, 18, -1, -1, -1, -1, -1, -1, -1, 9, 6},
//        {82, 19, -1, -1, -1, -1, -1, -1, -1, 10, 6},
//        {83, 20, -1, -1, -1, -1, -1, -1, -1, 11, 6},
//        {84, 21, -1, -1, -1, -1, -1, -1, -1, 0, 7}};

    
	private List<String> notes = new ArrayList<String>();
	private String title;
	private boolean debug = false;
	
	protected Music(String title){
		this.title=title;
	}
	
	protected Music(String title, boolean debug){
		this.title=title;
		this.debug=debug;
		if(debug) System.out.println("Titulo: "+title);
	}
	protected void addNote(String note){
		this.notes.add(note);
		if(debug) System.out.println("Nota "+notes.size()+": "+note);
	}
	
	public List<String> getNotes(){
		return this.notes;
	}
	
	protected String getTitle() {
		return title;
	}
	
	public int getNoteCount(){
		return this.notes.size();
	}
//	
//	public int[] createGenome(){
//		int[] genome = new int[notes.size()];
//		for(int i=0; i<genome.length;i++){
//			genome[i]=createGene(notes.get(i));
//		}
//		return genome;
//	}
//	
//	public int createGene(String n){
//		int freq,id,casa,corda;
//		String[] stepoctave = n.split("(?!^)");
//		String gene;
//		System.out.println(stepoctave[0]);
//		if(stepoctave[2].equals("#")){
//			freq = getIdFreqNota(stepoctave[1]+stepoctave[2], stepoctave[3]);
//		}else{
//			freq = getIdFreqNota(stepoctave[1],stepoctave[2]);
//		}
//		id = getIdPossivelNota(freq);
//		corda=matrizIdPosicoes[id][1];
//		casa=matrizIdPosicoes[id][2];
//		if(casa<10){
//			gene=""+freq+corda+"0"+casa;
//		}else{
//			gene=""+freq+corda+casa;
//		}
//		return Integer.parseInt(gene);
//	}
//	
//	private int getIdPossivelNota(int freq){
//		MersenneTwisterFast rand = new MersenneTwisterFast();
//		int id=0,index=0;
//		do{
//			index=rand.nextInt(matrizPosicoesNotasId[freq-40].length-2);
//			if(index>0){
//				id=matrizPosicoesNotasId[freq-40][index];
//			}
//		}while(id==-1);
//		return id;
//	}
//	
//
//	/**
//	 * @author João Ramos
//     *
//     * @param nota String com a Nota
//     * @param oitava String com a Oitava
//     * @return ID da frequencia daquela nota, não a frequencia em Hertz em si,
//     * apenas um id que diferencia uma nota de outra.
//     */
//    public static int getIdFreqNota(String nota, String oitava) {
//        int idFreqNota = -1, idNota = -1, numOitava, l;
//
//        if ("C".equals(nota)) {
//            idNota = 0;
//        } else if ("C#".equals(nota)) {
//            idNota = 1;
//        } else if ("D".equals(nota)) {
//            idNota = 2;
//        } else if ("D#".equals(nota)) {
//            idNota = 3;
//        } else if ("E".equals(nota)) {
//            idNota = 4;
//        } else if ("F".equals(nota)) {
//            idNota = 5;
//        } else if ("F#".equals(nota)) {
//            idNota = 6;
//        } else if ("G".equals(nota)) {
//            idNota = 7;
//        } else if ("G#".equals(nota)) {
//            idNota = 8;
//        } else if ("A".equals(nota)) {
//            idNota = 9;
//        } else if ("A#".equals(nota)) {
//            idNota = 10;
//        } else if ("B".equals(nota)) {
//            idNota = 11;
//        }
//
//        numOitava = Integer.parseInt(oitava);
//
//        for (l = 0; l < 44; l++) {
//            if (matrizPosicoesNotas[l][11] == idNota && matrizPosicoesNotas[l][12] == numOitava) {
//                idFreqNota = matrizPosicoesNotas[l][0];
//                break;
//            }
//        }
//        return idFreqNota;
//    }
//    
//	public static int mutateGene(int gene){
//		int freq,id,casa,corda;
//		String genestring = ""+gene;
//		char[] genechararray = genestring.toCharArray();
//		freq = (Character.getNumericValue(genechararray[0])*10)+Character.getNumericValue(genechararray[1]);
//		id = getIdPossivelNota(freq);
//		corda=matrizIdPosicoes[id][1];
//		casa=matrizIdPosicoes[id][2];
//		if(casa<10){
//			genestring=""+freq+corda+"0"+casa;
//		}else{
//			genestring=""+freq+corda+casa;
//		}
//		return Integer.parseInt(genestring);
//	}
	
	
}
package xmlMusicFiles;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;

public class MusicxmlParser {

	private static boolean debug = false;


	private FileReader getFileReader(String caminho) throws FileNotFoundException{
		FileReader filereader=null;
		// try{
			filereader = new FileReader(caminho);
		// }catch (FileNotFoundException e){
		// 	System.err.println("O arquivo especificado não existe");
		// 	System.exit(0);
		// }
		return filereader;
	}
	private XMLEventReader getXmlReader(String caminho) throws FileNotFoundException{
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLEventReader eventreader = null;
		try{
			eventreader = factory.createXMLEventReader(getFileReader(caminho));
		}catch (XMLStreamException e){
			e.printStackTrace();
		}
		return eventreader;
	}

	private String getElementData(XMLEventReader musicxml, XMLEvent event) throws XMLStreamException{
		event = getNextEvent(musicxml, event);
		return event.asCharacters().getData();
	}

	public Music readXml(String caminho) throws XMLStreamException,FileNotFoundException{
		String step="",octave="";
		XMLEventReader musicxml = getXmlReader(caminho);
		XMLEvent event = null;
		Attribute att;
		Music music = null;
		String part =null,pianopart = null,partname;
		boolean savenote = false;
		while(musicxml.hasNext()){
			event = getNextEvent(musicxml, event);
			if(event.isStartElement()){
				if(event.asStartElement().getName().getLocalPart().equals("score-part")){
					att = event.asStartElement().getAttributeByName(new QName("id"));
					part = att.getValue();
				}
				if(event.asStartElement().getName().getLocalPart().equals("part-name")){
					partname=getElementData(musicxml, event);
					if((partname.equals("Piano"))||(partname.equals("1"))){
						pianopart = part;
						String title = new File(caminho).getName();
						music = new Music(title, debug);
					}
				}
				if(event.asStartElement().getName().getLocalPart().equals("part")){
					att = event.asStartElement().getAttributeByName(new QName("id"));
					if(att.getValue().equals(pianopart)){
						savenote = true;
					}
				}
				if(event.asStartElement().getName().getLocalPart().equals("step")){
					if(savenote) step = getElementData(musicxml, event);
				}
				if(event.asStartElement().getName().getLocalPart().equals("octave")){
					if(savenote){
						octave = getElementData(musicxml, event);
						music.addNote(step+octave);
					}
				}
			}
			if(event.isEndElement()){
				if(event.asEndElement().getName().getLocalPart().equals("part")){
					savenote = false;
				}
			}
		}
		return music;
	}

	private XMLEvent getNextEvent(XMLEventReader musicxml, XMLEvent event) throws XMLStreamException {
			event = musicxml.nextEvent();
		return event;
	}

}

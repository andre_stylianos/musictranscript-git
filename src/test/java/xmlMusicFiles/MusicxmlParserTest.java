package xmlMusicFiles;

import static org.junit.Assert.*;

import javax.xml.stream.XMLStreamException;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.FileNotFoundException;

import xmlMusicFiles.Music;
import xmlMusicFiles.MusicxmlParser;

public class MusicxmlParserTest {

	private MusicxmlParser xmlparser;
	private Music music;
	@After
	public void tearDown(){
		music = null;
	}

	@Before
	public void initialize(){
		xmlparser = new MusicxmlParser();
	}

	@Test
	public void getMusicTest() {
		try {
			String caminho = "src/test/resources/libxml/waltz/A Maid That\'s Deep In Love.xml";
			music = xmlparser.readXml(caminho);
			assertEquals(69, music.getNoteCount());
			assertEquals("A5",music.getNotes().get(0));
			assertEquals("D5",music.getNotes().get(music.getNoteCount()-1));
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			fail("generated XMLStreamException");
			e.printStackTrace();
		} catch(FileNotFoundException e){
			fail("file not found");
			e.printStackTrace();
		}
	}

//	@Test
//	public void getMusicTesttwo() {
//		try{
//		music = xmlparser.readXml(System.getProperty("user.dir")+"/libxml/waltz/Best Mistake.xml");
//		assertEquals(1666, music.getNoteCount());
//		assertEquals("C4",music.getNotes().get(0));
//		assertEquals("D4",music.getNotes().get(music.getNoteCount()-1));
//		} catch (XMLStreamException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

}
